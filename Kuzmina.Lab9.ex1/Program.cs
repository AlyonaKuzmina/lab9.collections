﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text;
namespace Kuzmina.Lab9.ex1
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = @"C:\Users\LENOVO\Desktop\программирование\ЛАБ9 Коллекции\Kuzmina.Lab9.ex1\Table1.csv";
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Encoding encoding = Encoding.GetEncoding(1251);

            var lines = File.ReadAllLines(path, encoding);
            var persons = new Person[lines.Length - 1];

            for (int i = 1; i < lines.Length; i++)
            {
                var splits = lines[i].Split(';');
                var person = new Person();
                person.Id = Convert.ToInt32(splits[0]);
                person.Name = splits[1];
                person.Email = splits[2];
                person.Phone = splits[3];
                person.Age = Convert.ToInt32(splits[4]);
                person.City = splits[5];
                person.Street = splits[6];
                person.Tag = splits[7];
                person.Price = Convert.ToInt32(splits[8]);
                person.CustomerId = splits[9];
                person.ProductId = splits[10];

                persons[i - 1] = person;
            }

            //Задание 1
             Console.WriteLine("Задание 1");

                int chel = 0;
                for (var i = 0; i < persons.Length; i++)
                {
                    int k = persons.Count(s => s.Id == persons[i].Id);
                    if (k != 1)
                    {
                        Console.WriteLine("Записи по свойству Id не уникальны");
                        break;
                    }
                    chel++;
                }
                if (chel == persons.Length) Console.WriteLine("Записи по свойству Id уникальны");
                Console.WriteLine();

            //Задание 2
            Console.WriteLine("Задание 2");
            Console.WriteLine("Минимальный возраст: " + persons.Min(x => x.Age));
            Console.WriteLine();

            //Задание 3
            Console.WriteLine("Задание 3");
            var sorted = from x in persons
                         orderby x.Email
                         select x;

            var result = "resultsortedemail.csv";

            using (StreamWriter streamWriter = new StreamWriter(result, false, encoding))
            {
                streamWriter.WriteLine($"Id;Name;Email;Phone;Age;City;Street;Tag;Price;CustomerId;ProductId");

                foreach (var a in sorted)
                {
                    streamWriter.WriteLine(a.ToExcel());
                }
                foreach (Person person in sorted)
                Console.WriteLine(person.Id + " " + person.Name + " " + person.Email + " " + person.City + " " + person.Phone + " " + person.Age + " " + person.Street + " " + person.Tag + " " + person.Price + " " + person.CustomerId + " " + person.ProductId + " ");
            }
            Console.WriteLine();
            Console.WriteLine();

            //Задание 4
            Console.WriteLine("Задание 4");
            var selectedprice = from a in persons
                                where a.Price < 60000
                                select a;

            var result1 = "resultprice.csv";
            using (StreamWriter streamWriter = new StreamWriter(result1, false, encoding))
            {
                streamWriter.WriteLine($"Id;Name;Email;Phone;Age;City;Street;Tag;Price;CustomerId;ProductId");

                foreach (var m in selectedprice)
                {
                    streamWriter.WriteLine(m.ToExcel());
                }
            foreach (Person person in selectedprice)
                Console.WriteLine(person.Id + " " + person.Name + " " + person.Email + " " + person.City + " " + person.Phone + " " + person.Age + " " + person.Street + " " + person.Tag + " " + person.Price + " " + person.CustomerId + " " + person.ProductId + " ");
        }
            Console.WriteLine();
            Console.WriteLine();

            //Задание 5
            char[] symbols = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            string[] emails = { "c43b43@mail.ru", "dbw3@gmail.com", "rb3wa@mail.ru", "f4tb@gmail.com", "tb4eg4@yandex.ru", "gns4n@gmail.com", "tntste4t@mail.ru", "gf@gmail.com", "ecn246731ncq@gmail.com" };
            string[] names = { "Тимур Богомолов", "Даниил Кононов", "Ксения Латышева", "Ева Кузнецова", "Иван Бобров", "Мирон Козлов", "Анастасия Захарова", "Ольга Иванова", "Роман Рябов", "Мария Григорьева" };
            string[] cities = { "Москва", "Обнинск", "Омск", "Красноярск", "Краснодар", "Томск", "Новосибирск", "Санкт-Петербург", "Симферополь", "Севастополь" };
            string[] phones = { "(741)465-59-91", "(900)797-56-85", "(983)637-71-24", "(954)093-64-44", "(200)576-89-22", "(593)273-01-91", "(814)393-72-48", "(025)851-00-60", "(221)335-45-39", "(699)132-43-83" };
            string[] streets = { "Школьная улица", "Проспект маркса", "УЛица Ленина", "Набережная", "центральная улица", "Школьная улица", "Улица Саянская", "Улица Машиностроителей", "Улица Никольская" };
            string[] tags = { "Шапка", "Расческа", "Резинка", "Нож", "Сок", "Шоколад", "Бутылка", "Стул", "Подушка", "Рюкзак", "Картина", "Карты" };
            string[] ages = { "15", "16", "20", "23", "34", "37", "41", "49", "65", "70", "43", "27", "79", "83", "90" };
            var customId = new List<string>();
            var productID = new List<string>();
            Random random = new Random();
            //генерирация случайных записей для customerId 
            for (int j = 0; j < 10; j++)
            {
                string str = "";
                for (int i = 0; i < 10; i++)
                {
                    var newstr = symbols[random.Next(0, symbols.Length)];
                    str += newstr;
                }
                customId.Add(str);
            }
            //генерирация случайных записей для productID 
            for (int g = 0; g < 10; g++)
            {
                string stri = "";
                for (int o = 0; o < 10; o++)
                {
                    var newstri = symbols[random.Next(0, symbols.Length)];
                    stri += newstri;
                }
                productID.Add(stri);
            }
            var result2 = "result.csv";
            using (var writer = new StreamWriter(result2, true, encoding))

            {
                for (int l = persons.Length + 1; l < persons.Length + 5; l++)
                {
                    var NewRecord = new List<Person>()
                    {
                      new Person { Id = l, Name = names[random.Next(0, names.Length)], Email = emails[random.Next(0, emails.Length)], Phone = phones[random.Next(0, phones.Length)], Age = random.Next(0, ages.Length), City = cities[random.Next(0, cities.Length)], Street = streets[random.Next(0, streets.Length)], Tag = tags[random.Next(0, tags.Length)], Price = random.Next(200, 40000), CustomerId = customId[random.Next(0, customId.Count)], ProductId = productID[random.Next(0, productID.Count)] }
                    };
                    foreach (var n in NewRecord)
                    {
                        writer.WriteLine(n.ToExcel());
                    }
                }
            }
        }

    }
}



    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int Age { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Tag { get; set; }
        public int Price { get; set; }
        public string CustomerId { get; set; }
        public string ProductId { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\n Имя и фамилия: {Name}\n Электронный адрес : {Email}\n Номер телефона: {Phone}\n Возраст: {Age}\n Город: {City}\n Улица: {Street}\n Тэг:{Tag}\n Цена: {Price}\n Id покупателя: {CustomerId}\n Id товара: {ProductId}\n ";
        }
        public string ToExcel()
        {
            return $"{Id};{Name};{Email};{Phone};{Age};{City};{Street};{Tag};{Price};{CustomerId};{ProductId}";
        }
    }
